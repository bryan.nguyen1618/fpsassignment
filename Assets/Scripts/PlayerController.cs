using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : BaseEntity
{
    public Transform characterCam;
    [SerializeField]
    protected WeaponController _weaponController;
    [SerializeField]
    protected Animator _playerAnimator;

    [SerializeField]
    PlayerHUDController _playerHUDController;

    // Start is called before the first frame update
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameLogicManager.Singleton.GameLogic.IsEnd())
        {
            return;
        }
        if (IsAlive)
        {
            Vector3 inputMove = Vector3.zero;
            inputMove.x = Input.GetAxis("Horizontal");
            inputMove.z = Input.GetAxis("Vertical");
            UpdateMovement(inputMove);

            Vector3 inputLook = Vector3.zero;
            inputLook.x = -Input.GetAxis("Mouse Y");
            inputLook.y = Input.GetAxis("Mouse X");

            UpdateRotation(inputLook);

            UpdateShoot(Input.GetButton("Fire1"));

        }
        UpdateHUD();
    }

    void FixedUpdate()
    {

    }

    void UpdateMovement(Vector3 input)
    {
        Vector3 direction = characterCam.transform.right * input.x + characterCam.transform.forward * input.z;

        transform.position += direction.normalized * _speed * Time.deltaTime;

        // update animator
        _playerAnimator.SetBool("IsMoving", direction.magnitude > 0f);
    }

    void UpdateRotation(Vector3 input)
    {
        float rotX = characterCam.localEulerAngles.x + input.x*Time.deltaTime * 500f;
        rotX = rotX >= 270 ? rotX - 360 : rotX;

        // clamp rotation X
        rotX = Mathf.Clamp(rotX, -50, 50);
        characterCam.localRotation = Quaternion.Euler(new Vector3(rotX,0,0));

        float rotY = transform.rotation.eulerAngles.y + input.y * _speedRotation * Time.deltaTime;
        transform.rotation = Quaternion.Euler(new Vector3(0,rotY,0));
    }

    void UpdateShoot(bool isShooting)
    {
        if (isShooting)
        {
            _weaponController.PressShoot(GetShootStartPoint(), GetShootDirect());
        }
        else
        {
            _weaponController.ReleaseShoot();
        }


        //update animator
        _playerAnimator.SetBool("IsShooting", _weaponController.IsShooting);
        _playerAnimator.SetBool("IsReloading", _weaponController.IsReloading);
        
    }


    void UpdateHUD()
    {
        _playerHUDController.SetAmmo(_weaponController.LoadedAmmo, _weaponController.RemainingAmmo);
        _playerHUDController.HealthBarUIController.SetHP(PercentHealthPoint);
        _playerHUDController.EnableAnimCrosshair(_weaponController.IsShooting);
    }

    Vector3 GetShootDirect()
    {
        return characterCam.forward;
    }

    Vector3 GetShootStartPoint()
    {
        return characterCam.position + GetShootDirect() * 0.5f;
    }
}
