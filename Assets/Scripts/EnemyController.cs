using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : BaseEntity, IAIBehavior
{
    public float RangeDealDamage = 2f;
    
    [SerializeField]
    protected ParticleSystem _takeDownFx;

    protected BaseEntity _target = null;

    protected Animator _animator;

    protected Dictionary<int, AIState> _mapAIState = new Dictionary<int, AIState>();

    private float _resetTakingDamageTimer = 0;

    private Rigidbody _rigidbody;

    public override Vector3 Velocity { get => _rigidbody.velocity; set => _rigidbody.velocity = value; }

    // Start is called before the first frame update
    void Start()
    {
        Spawn();
        _animator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody>();
        // Find target
        FindTarget();

        _mapAIState[Animator.StringToHash("Base Layer.Idle")] = new AIStateIdle(this);
        _mapAIState[Animator.StringToHash("Base Layer.Chase")] = new AIStateChase(this, this);
        _mapAIState[Animator.StringToHash("Base Layer.Attack")] = new AIStateAttack(this, this);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameLogicManager.Singleton.GameLogic.IsEnd())
        {
            //disable
            _animator.SetBool("IsEndGame", true);
            return;
        }
        if (IsAlive)
        {
            float distanceToTarget = float.MaxValue;
            if (_target)
            {
                distanceToTarget = Vector3.Distance(transform.position, _target.transform.position);
            }

            _animator.SetFloat("DistanceToTarget", distanceToTarget);

            int stateHash = _animator.GetCurrentAnimatorStateInfo(0).fullPathHash;
            _mapAIState[stateHash]?.UpdateLogic();

            if (_resetTakingDamageTimer > 0)
            {
                _resetTakingDamageTimer -= Time.deltaTime;
            }
            _animator.SetBool("IsTakingDamage", _resetTakingDamageTimer > 0);
        }
    }

    // Fired by Animation Event 'Melee_OneHanded'
    public void OnAnimDealDamage()
    {
        //check target
        if (!_target)
        {
            return;
        }

        float distance = Vector3.Distance(_target.transform.position, transform.position);
        if (distance <= RangeDealDamage)
        {
            _target.TakeDamage(_damage);
        }

    }

    public BaseEntity FindTarget()
    {
        if (!_target)
        {
            _target = GameObject.Find("MyPlayer")?.GetComponent<BaseEntity>();
        }
        return _target;
    }

    public override void TakeDamage(int damage)
    {
        base.TakeDamage(damage);
        EnemyHUDController.Singleton.HealthBarUIController.SetHP(PercentHealthPoint);
        _resetTakingDamageTimer = 5;
    }

    public override void OnDied()
    {
        base.OnDied();
        if(_takeDownFx)
        {
            //_takeDownFx.transform.position = transform.position;
            _takeDownFx.Play();
        }

        StartCoroutine(DelayDisable());
    }

    IEnumerator DelayDisable()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }
}
