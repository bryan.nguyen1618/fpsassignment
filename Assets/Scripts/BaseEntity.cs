using UnityEngine;

public class BaseEntity : MonoBehaviour
{
    [SerializeField]
    protected int _healthPoint = 1000;

    [SerializeField]
    protected int _maxHealthPoint = 1000;

    [SerializeField]
    protected float _speed = 10f;

    [SerializeField]
    protected float _speedRotation = 10f;

    [SerializeField]
    public int _damage = 0;

    public bool IsAlive { get => _healthPoint > 0; }

    public int HealthPoint { get => _healthPoint; }
    public int MaxHealthPoint { get => _maxHealthPoint; }
    public float PercentHealthPoint { get => (float)_healthPoint / _maxHealthPoint; }
    public virtual Vector3 Velocity { get; set; }
    public float Speed { get => _speed; }
    public float SpeedRotation { get => _speedRotation; }

    public virtual void Spawn()
    {
        _healthPoint = _maxHealthPoint;
    }

    public virtual void TakeDamage(int damage)
    {
        if (IsAlive)
        {
            _healthPoint -= damage;
            _healthPoint = Mathf.Max(_healthPoint, 0);

            if (!IsAlive)
            {
                OnDied();
            }
        }
    }

    public virtual void OnDied()
    {
        // do something
    }
}