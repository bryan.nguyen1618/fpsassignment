using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    // Weapon attribute
    public int RatePerSec = 100;
    public int Damage = 10;
    public int LoadedAmmo = 30;
    public int RemainingAmmo = int.MaxValue;

    public int MaxLoadedAmmo = 30;
    public float ReloadingTime = 2f;

    [SerializeField]
    protected Light _muzzleLight;
    [SerializeField]
    protected ParticleSystem _muzzleParticle;
    [SerializeField]
    protected ParticleSystem _sparkParticle;

    private float _intervalFiring = 0.0f;
    private float _intervalReloading = 0.0f;


    public bool IsShooting { get; protected set; }
    public bool IsReloading { get; protected set; }

    // Start is called before the first frame update
    void Start()
    {
        var emission = _muzzleParticle.emission;
        emission.rateOverTime = RatePerSec;

        emission = _sparkParticle.emission;
        emission.rateOverTime = RatePerSec;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
    public void PressShoot(Vector3 startPosition, Vector3 shootDirection)
    {
        if (IsReloading) return;

        if (LoadedAmmo <= 0)
        {
            StartCoroutine(DelayReload());
            return;
        }

        _intervalFiring -= Time.deltaTime;
        if (_intervalFiring <= 0)
        {
            LoadedAmmo--;
            
            // fire
            Debug.DrawRay(startPosition, shootDirection * 100f, Color.red, 2);
            _intervalFiring = 1f / RatePerSec;

            ShootLogic(_muzzleParticle.transform.position, shootDirection);

            //effect
            DoShootingEffect(true);
        }
        else
        {
            //effect
            DoShootingEffect(false);
        }
        IsShooting = true;
    }

    public void ReleaseShoot()
    {
        _intervalFiring = 0;

        //effect
        DoShootingEffect(false);

        IsShooting = false;
    }

    IEnumerator DelayReload()
    {
        DoShootingEffect(false);
        IsReloading = true;
        yield return new WaitForSeconds(ReloadingTime);
        Reload();
        IsReloading = false;
    }

    public void Reload()
    {
        RemainingAmmo -= MaxLoadedAmmo;
        LoadedAmmo = MaxLoadedAmmo;
    }

    public void ShootLogic(Vector3 start, Vector3 end)
    {
        RaycastHit hit;
        if(Physics.Raycast(start, end.normalized, out hit, 100f))
        {
            //hit.collider.gameObject.transform.par
            if (hit.collider.tag == "Zombie" || hit.collider.tag == "ZombieBoss")
            {
                var enemyController = hit.collider.GetComponentInParent<BaseEntity>();
                enemyController?.TakeDamage(Damage);
            }
            BulletFXManager.Singleton.PlayFX(hit.collider.tag, hit.point, hit.normal);
        }
    }

    public void DoShootingEffect(bool val)
    {
        _muzzleLight.enabled = val;
        if (val)
        {
            _muzzleParticle.Play();
            _sparkParticle.Play();
        }
        else
        {
            _muzzleParticle.Stop();
            _sparkParticle.Stop();
        }
    }
}
