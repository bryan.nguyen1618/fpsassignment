using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIManager : MonoBehaviour
{
    [SerializeField]
    protected GameObject _missionMsg;
    [SerializeField]
    protected GameObject _missionStatusPanel;
    [SerializeField]
    protected GameObject _missionStatusCompleted;
    [SerializeField]
    protected GameObject _missionStatusFailed;

    private static GameUIManager _instance;

    public GameUIManager Singleton { get => _instance; }

    private void Awake()
    {
        if (!_instance)
        {
            _instance = this;
        }
        else
        {
            throw new System.Exception("Error: more than one instance of GameUIManager");
        }

    }

    public void ShowStartGame()
    {
        _missionMsg.SetActive(true);
    }

    public void ShowMissionComplete(bool val)
    {
        _missionStatusPanel.SetActive(val);
        _missionStatusCompleted.SetActive(val);
    }

    public void ShowMissionFailed(bool val)
    {
        _missionStatusPanel.SetActive(val);
        _missionStatusFailed.SetActive(val);
    }
}
