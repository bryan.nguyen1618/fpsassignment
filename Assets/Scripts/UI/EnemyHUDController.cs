using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHUDController : MonoBehaviour
{
    [SerializeField]
    protected HealthBarUIController _healthBarController;

    public HealthBarUIController HealthBarUIController { get => _healthBarController; }


    private static EnemyHUDController _instance;
    public static EnemyHUDController Singleton { get => _instance; }

    private void Awake()
    {
        if (!_instance)
        {
            _instance = this;
        }
        else
        {
            throw new System.Exception("More than one instance EnemyHUDController");
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
