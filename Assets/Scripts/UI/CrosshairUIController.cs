using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosshairUIController : MonoBehaviour
{
    [SerializeField]
    protected Animation _animation;

    public void PlayAnim()
    {
        _animation.Play("crosshair_shooting");
    }

    public void StopAnim()
    {
        _animation.Play("crosshair_idle");
    }
}
