using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarUIController : MonoBehaviour
{
    public bool IsAlwayDisplay = false;
    public float DisplayDuration = 2f;

    [SerializeField]
    protected Slider _healthBarSlider;

    private float displayTimer = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (IsAlwayDisplay)
        {
            _healthBarSlider.gameObject.SetActive(true);
            return;
        }

        displayTimer += Time.deltaTime;
        if (displayTimer >= DisplayDuration)
        {
            _healthBarSlider.gameObject.SetActive(false);
        }
    }

    public void SetHP(float percentHP)
    {
        _healthBarSlider.value = percentHP;
        displayTimer = 0;
        _healthBarSlider.gameObject.SetActive(true);
    }
}
