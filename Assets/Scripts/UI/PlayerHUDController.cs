using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUDController : MonoBehaviour
{
    [SerializeField]
    protected Animation _crosshairAnimation;

    [SerializeField]
    protected Text _loadedAmmoTxt;

    [SerializeField]
    protected Text _remainingAmmoTxt;

    [SerializeField]
    protected HealthBarUIController _healthBarController;

    public HealthBarUIController HealthBarUIController { get => _healthBarController; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void EnableAnimCrosshair(bool val)
    {
        _crosshairAnimation.Play(val ? "crosshair_shooting" : "crosshair_idle");
    }

    public void SetAmmo(int loadedAmmo, int remainingAmmo)
    {
        _loadedAmmoTxt.text = loadedAmmo.ToString();
        _remainingAmmoTxt.text = remainingAmmo > 999 ? "999+" : remainingAmmo.ToString();
    }


}
