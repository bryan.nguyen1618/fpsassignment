using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogicManager : MonoBehaviour
{
    [SerializeField]
    protected GameUIManager _gameUIManager;

    protected IGameLogic _gameLogic;
    public IGameLogic GameLogic { get => _gameLogic; }

    private BaseEntity _player, _boss;

    private static GameLogicManager _instance;
    public static GameLogicManager Singleton { get => _instance; }
    private void Awake()
    {
        if(!_instance)
        {
            _instance = this;
        }
        else
        {
            throw new System.Exception("Error: more than 1 instance of GameLogicManager");
        }


        _player = GameObject.Find("MyPlayer").GetComponent<BaseEntity>();
        _boss = GameObject.Find("SA_Zombie_Boss").GetComponent<BaseEntity>();
        var gameLogic = new SimpleGameLogic(_player, _boss);
        gameLogic.OnStart += OnGameLogicStart;
        gameLogic.OnEnd += OnGameLogicEnd;

        _gameLogic = gameLogic;
    }
    // Start is called before the first frame update
    void Start()
    {
        _gameLogic.Start();
    }

    // Update is called once per frame
    void Update()
    {
        _gameLogic.UpdateLogic();
    }

    void OnGameLogicStart()
    {
        Debug.Log("On Game Logic Start");
        _gameUIManager.ShowStartGame();

    }

    void OnGameLogicEnd()
    {
        Debug.Log("On Game Logic End");
        if (_gameLogic.GetWinningPlayer() == _player)
        {
            _gameUIManager.ShowMissionComplete(true);
        }
        else if (_gameLogic.GetWinningPlayer() == _boss)
        {
            _gameUIManager.ShowMissionFailed(true);
        }
    }
}

public interface IGameLogic
{
    public void Start();
    public void UpdateLogic();
    public bool IsEnd();
    public BaseEntity GetWinningPlayer();
}

public delegate void GameLogicCallback();
public class SimpleGameLogic : IGameLogic
{
    protected BaseEntity _player, _boss;

    protected BaseEntity _winningPlayer;
    protected bool _isEndGame = false;

    public GameLogicCallback OnEnd;
    public GameLogicCallback OnStart;

    public SimpleGameLogic(BaseEntity player, BaseEntity boss)
    {
        _player = player;
        _boss = boss;
    }


    bool IGameLogic.IsEnd()
    {
        return _isEndGame;
    }

    void IGameLogic.UpdateLogic()
    {
        if (_isEndGame) return;
        if (!_player.IsAlive || !_boss.IsAlive)
        {
            _isEndGame = true;

            _winningPlayer = _player.IsAlive ? _player : _boss;

            OnEnd?.Invoke();
        }
    }

    void IGameLogic.Start()
    {
        OnStart?.Invoke();
    }

    public BaseEntity GetWinningPlayer()
    {
        return _winningPlayer;
    }
}