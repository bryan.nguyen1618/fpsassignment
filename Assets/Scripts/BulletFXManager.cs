using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFXManager : MonoBehaviour
{
    [SerializeField]
    protected ParticleSystem _bloodBossFX;

    [SerializeField]
    protected ParticleSystem _bloodZombieFX;

    [SerializeField]
    protected ParticleSystem _hitSandFX;

    [SerializeField]
    protected ParticleSystem _hitWoodFX;

    private static BulletFXManager _instance;
    public static BulletFXManager Singleton { get => _instance; }

    private void Awake()
    {
        if (!_instance)
        {
            _instance = this;
        }
        else
        {
            throw new System.Exception("Error: more than 1 instance of BulletFXManager");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public ParticleSystem GetFXByTag(string tagObj)
    {
        switch(tagObj)
        {
            case "ZombieBoss":
                return _bloodBossFX;
            case "Zombie":
                return _bloodZombieFX;
            case "Sand":
                return _hitSandFX;
            case "Wood":
                return _hitWoodFX;
            default:
                return null;
        }
    }

    public void PlayFX(string tagObj, Vector3 point, Vector3 normal)
    {
        Debug.Log("PlayFX" + tagObj);
        var particle = GetFXByTag(tagObj);
        if (particle)
        {
            particle.transform.position = point;
            particle.transform.rotation = Quaternion.LookRotation(normal);
            particle.Play();
        }
        
    }
}
