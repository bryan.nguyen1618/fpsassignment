using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAIBehavior
{
    public BaseEntity FindTarget();
}

public abstract class AIState
{
    protected BaseEntity _entity;
    public AIState(BaseEntity obj)
    {
        _entity = obj;
    }

    abstract public void UpdateLogic();
}
public class AIStateIdle : AIState
{
    public AIStateIdle(BaseEntity obj) : base(obj)
    {

    }

    public override void UpdateLogic()
    {
        //Debug.Log("[AIStateIdle::UpdateLogic]");
        _entity.Velocity = Vector3.zero;
    }
}


public class AIStateChase : AIState
{
    protected IAIBehavior _objBehavior;
    public AIStateChase(BaseEntity obj, IAIBehavior behavior) : base(obj)
    {
        _objBehavior = behavior;
    }

    public override void UpdateLogic()
    {
  
        var target = _objBehavior.FindTarget();
        //_objBehavior.MoveTo(target.transform.position);

        Vector3 dir = (target.transform.position - _entity.transform.position).normalized;
        dir.y = 0;
        Vector3 velocity = new Vector3(dir.x, 0, dir.z);
        velocity *= _entity.Speed;
        velocity.y = _entity.Velocity.y;
        //transform.position += dir.normalized * _speed * Time.deltaTime;
        _entity.Velocity = velocity;

        //update rotation
        _entity.transform.rotation = Quaternion.Lerp(_entity.transform.rotation, Quaternion.LookRotation(dir.normalized, _entity.transform.up), _entity.SpeedRotation * Time.deltaTime);
    }
}

public class AIStateAttack : AIState
{
    protected IAIBehavior _objBehavior;

    public AIStateAttack(BaseEntity obj, IAIBehavior behavior) : base(obj)
    {
        _objBehavior = behavior;
    }

    public override void UpdateLogic()
    {
        //Debug.Log("[AIStateAttack::UpdateLogic]");
        _entity.Velocity = Vector3.zero;
        var target = _objBehavior.FindTarget();
        Vector3 dir = target.transform.position - _entity.transform.position;
        dir.y = 0;
        _entity.transform.rotation = Quaternion.Lerp(_entity.transform.rotation, Quaternion.LookRotation(dir.normalized, _entity.transform.up), _entity.SpeedRotation * Time.deltaTime);
    }
}
